/* eslint-disable  func-names */
/* eslint-disable  no-console */

const Alexa = require('ask-sdk-core');
const {
    firstWord,
    isCorrect,
    imageUrl,
    setQuestionIndex,
    getQuestionIndex,
	getUsername,
	setUsername,
	getScore,
	incrementScore
} = require('tbd')
const {words} = require('./words')

const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'LaunchRequest';
    },
    handle(handlerInput) {
        const speechText = 'Welcome to the quiz! Say start the game to start the quiz';

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .withStandardCard('Spelling with Scholastic', speechText, 'https://hsc.apsva.us/wp-content/uploads/sites/20/2017/09/Welcome-02-web-version.jpg')
            .getResponse();
    },
};

const StartGameIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest' &&
            handlerInput.requestEnvelope.request.intent.name === 'StartGameIntent';
    },
    handle(handlerInput) {
        setQuestionIndex(handlerInput,0);
        const speechText = 'What is your name?';

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .withStandardCard('Spelling with Scholastic', ' ', 'https://s3.amazonaws.com/schl-hosting-prod-da-content/srp/dev/team4/hello.jpg')
            .getResponse();
    },
};


const HelpIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest' &&
            handlerInput.requestEnvelope.request.intent.name === 'AMAZON.HelpIntent';
    },
    handle(handlerInput) {
        const speechText = 'Spell the word being shown';

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .withSimpleCard('Spelling with Scholastic', speechText)
            .getResponse();
    },
};


const FallbackIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest' &&
            handlerInput.requestEnvelope.request.intent.name === 'AMAZON.FallbackIntent';
    },
    handle(handlerInput) {
        const speechText = 'Sorry I don\t understand';

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .withSimpleCard('Spelling with Scholastic', speechText)
            .getResponse();
    },
};


const CancelAndStopIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest' &&
            (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.CancelIntent' ||
                handlerInput.requestEnvelope.request.intent.name === 'AMAZON.StopIntent');
    },
    handle(handlerInput) {
        const speechText = 'Thanks for playing.  Goodbye!';

        return handlerInput.responseBuilder
            .speak(speechText)
            .withSimpleCard('Spelling with Scholastic', speechText)
            .getResponse();
    },
};

const AnswerIntentHandler = {
        canHandle(handlerInput) {
            return handlerInput.requestEnvelope.request.type === 'IntentRequest' &&
                (handlerInput.requestEnvelope.request.intent.name === 'AnswerIntent' ||
                    handlerInput.requestEnvelope.request.intent.name === 'IDKIntent');
        },
        handle(handlerInput) {
            var speechText;
            if (handlerInput.requestEnvelope.request.intent.name === 'AnswerIntent') {
                var correct = isCorrect(handlerInput);
                if (correct) {
                    speechText = 'Good, that is correct. ';
			incrementScore(handlerInput) ;
                } else {
                    speechText = 'Whoops, that is incorrect. ';
                } 
	    } else {
                speechText = 'Ok let\s carry on.';
            }
	    var qi = getQuestionIndex(handlerInput) ;
	    qi++ ;
	setQuestionIndex(handlerInput,qi) ;
		var nextWord ;
		var gameOver = false ;
		if (qi < words.length) {
		    nextWord = words[qi].word;
		}
		else {
			nextWord = 'Great job ' + getUsername(handlerInput) + '! You scored ' + getScore(handlerInput) + ' out of ' + words.length + '. Game Over!';
		gameOver = true ;

		}
		speechText += (gameOver == true ? ' ' : 'Onto the next word. Spell ') + nextWord ;
            return handlerInput.responseBuilder
                .speak(speechText)
                .reprompt(speechText)
                .withStandardCard('Spelling with Scholastic', ' ', gameOver == true ? 'https://pbs.twimg.com/profile_images/657603448553127936/D7T9j7Pk_400x400.png' : imageUrl(nextWord))
                .getResponse();
            },
        };


        const SessionEndedRequestHandler = {
            canHandle(handlerInput) {
                return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
            },
            handle(handlerInput) {
                console.log(`Session ended with reason: ${handlerInput.requestEnvelope.request.reason}`);
                return handlerInput.responseBuilder.getResponse();
            },
        };

const UsernameIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest' &&
            (handlerInput.requestEnvelope.request.intent.name === 'UsernameIntent') ;
    },
    handle(handlerInput) {
	    // Get username
	    var username = handlerInput.requestEnvelope.request.intent.slots.username.value;
	    setUsername(handlerInput, username) ;
        const speechText = 'Ok ' + username + ', we\'ll start. I\'ll show you a word, and you\'ll spell it back. Spell ' + firstWord();

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .withStandardCard('Spelling Game', ' ', imageUrl(firstWord()))
            .getResponse();
    },
};

        const ErrorHandler = {
            canHandle() {
                return true;
            },
            handle(handlerInput, error) {
                console.log(`Error handled: ${error.message}`);

                return handlerInput.responseBuilder
                    .speak('That is the incorrect answer')
                    .reprompt('That is the incorrect answer')
                    .getResponse();
            },
        };

        const skillBuilder = Alexa.SkillBuilders.custom();

        exports.handler = skillBuilder
        .addRequestHandlers(
            LaunchRequestHandler,
            StartGameIntentHandler,
            HelpIntentHandler,
            AnswerIntentHandler,
            CancelAndStopIntentHandler,
	    UsernameIntentHandler,
            SessionEndedRequestHandler
        )
        .addErrorHandlers(ErrorHandler)
        .lambda();