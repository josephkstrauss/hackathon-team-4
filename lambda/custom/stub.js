const {firstWord, isCorrect, setQuestionIndex, getQuestionIndex, getSessionAttribute, userAnswer, imageUrl, normalize,
  setUsername, getUsername, getScore, incrementScore} = require('./tbd')
function test(word, index) {
	var sa = {questionIndex: index}
	return {
	  attributesManager: {
		setSessionAttributes(nsa){
		  sa = nsa
		},
		getSessionAttributes() {
		  return sa
		}
	  },
	  requestEnvelope: {
		request: {
		  intent: {
		    slots: {
			  Answer: {
			    value: word
			  }
			}
		  }
		}
	  }
    }
}

function expectEquals(message, expected, actual){
	if(JSON.stringify(expected) === JSON.stringify(actual)){
		console.log(message + ' - Succcess')
	} else {
		console.log('ERRROR:   ' + message)
		console.log('Expected: ' + JSON.stringify(expected))
		console.log('Actual:   ' + JSON.stringify(actual))
	}
}

expectEquals('Test firstWord', 'lion', firstWord())
expectEquals('Normalize word', 'lion', normalize('LION'))
expectEquals('Normalize word', 'lion', normalize('l. i. o. n.'))
expectEquals('Normalize word', 'lion', normalize('L.I.O.N'))
expectEquals('Normalize word', 'lion', normalize('l.i.o.n.'))
expectEquals('Normalize word', 'lion', normalize('LION'))
expectEquals('Test getSessionAttribute', {questionIndex: '9'}, test('d. o. g.', '9').attributesManager.getSessionAttributes())
expectEquals('Test userAnswer', 'dog', userAnswer(test('dog', '9')))
let hi = test('d. o. g.', '9')
expectEquals('Test setQuestionIndex 9', 9, getQuestionIndex(hi))
setQuestionIndex(hi,3)
expectEquals('Test setQuestionIndex 3', 3, getQuestionIndex(hi))
expectEquals('Test getQuestionIndex 7', 7, getQuestionIndex(test('d. o. g.', '9')) - 2)
expectEquals('Test isCorrect - lion', true, isCorrect(test('l. i. o. n.', '0')))
expectEquals('Test isCorrect - lion wrong index', false, isCorrect(test('l. i. o. n.', '1')))
expectEquals('Test isCorrect - chicken', true, isCorrect(test('c. h. i. c. k. e. n.', '1')))
expectEquals('Test isCorrect - dolphin', true, isCorrect(test('d. o. l. p. h. i. n.', '2')))
expectEquals('Test isCorrect - dog', true, isCorrect(test('d. o. g.', '3')))
expectEquals('Test isCorrect - clifford', true, isCorrect(test('c. l. i. f. f. o. r. d.', '3')))
expectEquals('Test isCorrect - dolfin', false, isCorrect(test('d. o. l. f. i. n.', '2')))
setUsername(hi, 'John')
expectEquals('Test setUsername', 'John', getSessionAttribute(hi, 'username'))
expectEquals('Test setUsername does not clobber other attributes', '3', getSessionAttribute(hi, 'questionIndex'))
expectEquals('Test getUsername', 'John', getUsername(hi))
expectEquals('Test imageUrl', 'https://s3.amazonaws.com/schl-hosting-prod-da-content/srp/dev/team4/Dog.jpg', imageUrl('dog'))
expectEquals('Test getScore', 0, getScore(hi))
expectEquals('Test score', undefined, getSessionAttribute(hi, 'score'))
incrementScore(hi)
expectEquals('Test incrementScore', '1', getSessionAttribute(hi, 'score'))
incrementScore(hi)
expectEquals('Test incrementScore', '2', getSessionAttribute(hi, 'score'))
