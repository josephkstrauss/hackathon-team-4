const {words} = require('./words')
module.exports = {
	firstWord() {
		return words[0].word
	},
	isCorrect(handlerInput){
		const { attributesManager, requestEnvelope } = handlerInput
		const {sessionAttributes } = attributesManager
		let questionIndex = module.exports.getQuestionIndex(handlerInput)
		const userAnswer = module.exports.normalize(module.exports.userAnswer(handlerInput))
		return words[questionIndex].word === userAnswer ||
		  (words[questionIndex].word == 'dog' && userAnswer === 'clifford')
	},
	getQuestionIndex(handlerInput) {
	  return parseInt(module.exports.getSessionAttribute(handlerInput, 'questionIndex'))
	},
	setQuestionIndex(handlerInput, questionIndex) {
	  module.exports.setSessionAttribute(handlerInput, 'questionIndex', questionIndex + '')
	},
	getSessionAttribute(handlerInput, attribute){
	  return handlerInput.attributesManager.getSessionAttributes()[attribute]
	},
	setSessionAttribute(handlerInput, attribute, value){
	  let sa = handlerInput.attributesManager.getSessionAttributes()
	  sa[attribute] = value
	  handlerInput.attributesManager.setSessionAttributes(sa)
	},
	userAnswer(handlerInput){
		return handlerInput.requestEnvelope.request.intent.slots.Answer.value
	},
	normalize (word) {
		return word.replace(/[\. ]/g, '').toLowerCase()
	},
	dotify (word) {
		return word.replace(/(.)/g, '$1. ').trim()
	},
	getUsername(handlerInput) {
	  return module.exports.getSessionAttribute(handlerInput, 'username')
	},
	setUsername(handlerInput, value){
      module.exports.setSessionAttribute(handlerInput, 'username', value)
	},
	imageUrl(word){
		return 'https://s3.amazonaws.com/schl-hosting-prod-da-content/srp/dev/team4/' + (word.replace(/(.)/, function(e){return e.toUpperCase()})) + '.jpg'
	},
	getScore(handlerInput){
		const cv = module.exports.getSessionAttribute(handlerInput, 'score')
		return cv ? parseInt(cv) : 0
	},
	incrementScore(handlerInput){
	  module.exports.setSessionAttribute(handlerInput, 'score', (module.exports.getScore(handlerInput) + 1) + '')
	}
}